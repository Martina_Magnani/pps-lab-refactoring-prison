package main;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import controller.Implementations.LoginControllerImpl;
import model.Implementations.CellImpl;
import model.Implementations.GuardImpl;
import model.Interfaces.Cell;
import model.Interfaces.Guard;
import model.Interfaces.Person;
import view.Interfaces.LoginView;

/**
 * The main of the application.
 */
public final class Main {

	/**
     * Program main, this is the "root" of the application.
     * @param args
     * unused,ignore
     */
	 public static void main(final String... args){
		 //creo cartella in cui mettere i dati da salvare
		 String directory = "res";
		 new File(directory).mkdir();

		 //creo file per le guardie
		 File fileGuards = new File("res/GuardieUserPass.txt");
		 //se il file non è stato inizializzato lo faccio ora
		 if (fileGuards.length() == 0) {
			 try {
				initializeGuards(fileGuards);
			} catch (IOException e) {
				e.printStackTrace();
			}
		 }

		 //leggo il file contenente le celle
		 File fileCells = new File("res/Celle.txt");
		 //se il file non è ancora stato inizializzato lo faccio ora
		 if (fileCells.length() == 0) {
			 try {
				initializeCells(fileCells);
			} catch (IOException e) {
				e.printStackTrace();
			}
		 }
		 
		 //chiamo controller e view del login
		 new LoginControllerImpl(new LoginView());
	 }
	 
	 /**
	  * metodo che inizializza le celle
	  * @param fileCells file in cui creare le celle
	  * @throws IOException
	  */
	 static void initializeCells(File fileCells) throws IOException{

		 List<Cell> listCells = new ArrayList<>();
		 Cell cell;
		 for (int i = 0; i < 50; i++) {
			 if (i < 20) {
				  cell = new CellImpl(i, "Primo piano", 4);
			 }
			 else
				 if (i < 40) {
					  cell = new CellImpl(i, "Secondo piano", 3);
				 }
				 else
					 if (i < 45) {
					  cell = new CellImpl(i, "Terzo piano", 4);
				 }
					 else{
						  cell = new CellImpl(i, "Piano sotterraneo, celle di isolamento", 1);
					 }
			 listCells.add(cell);
		 }

		 writeCellToFile(fileCells, listCells);
	 }
	 
	 /**
	  * metodo che inizializza le guardie
	  * @param fileGuards file in cui creare le guardie
	  * @throws IOException
	  */
	 static void initializeGuards(File fileGuards) throws IOException {

	     String datePattern = "MM/dd/yyyy";
		 SimpleDateFormat dateFormat = new SimpleDateFormat(datePattern);
		 Date date = null;
		try {
			date = dateFormat.parse("01/01/1980");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		List<Person> listGuards = new ArrayList<>();
		Guard firstGuard = new GuardImpl("Oronzo","Cantani",date,1,"0764568",1,"ciao01");
		listGuards.add(firstGuard);
		Guard secondGuard = new GuardImpl("Emile","Heskey",date,2,"456789",2,"asdasd");
		listGuards.add(secondGuard);
		Guard thirdGuard = new GuardImpl("Gennaro","Alfieri",date,3,"0764568",3,"qwerty");
		listGuards.add(thirdGuard);

		writePersonToFile(fileGuards, listGuards);
	 }


	 private static void writeCellToFile (File fileName, List<Cell> cells) throws IOException{
         FileOutputStream fo = new FileOutputStream(fileName);
         ObjectOutputStream os = new ObjectOutputStream(fo);
         os.flush();
         fo.flush();
         for (Cell c1 : cells) {
             os.writeObject(c1);
         }
         os.close();
	 }

	 private static void writePersonToFile(File fileName, List<Person> persons) throws IOException {
		 FileOutputStream fo = new FileOutputStream(fileName);
		 ObjectOutputStream os = new ObjectOutputStream(fo);
		 os.flush();
		 fo.flush();
		 for (Person g : persons) {
			 os.writeObject(g);
		 }
		 os.close();
	 }
	 
}
